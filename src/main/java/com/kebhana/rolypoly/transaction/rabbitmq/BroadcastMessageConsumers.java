package com.kebhana.rolypoly.transaction.rabbitmq;

import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.kebhana.rolypoly.transaction.model.Transaction;

import lombok.extern.slf4j.Slf4j;

@Component
@Slf4j
public class BroadcastMessageConsumers {
	
//	//수신서비스 
//	@RabbitListener(queues = "${prop.rabbit.queue.order}" )
//	public void receiveMessageFromDirectExchangeWithOrderQueue(Transaction message) {
//		log.debug("CHARGE_ORDER_QUEUE Receive : "+message.toString());
//		//todo data를 수신해서 로직 처리 
//	}
	
//	@RabbitListener(queues = {CHARGE_COMPLETE_QUEUE})
//	public void receiveMessageFromDirectExchangeWithCompleteQueue(SampleUser message) {
//		log.debug(" CHARGE_COMPLETE_QUEUE Receive : "+message.toString());
//	}
}
