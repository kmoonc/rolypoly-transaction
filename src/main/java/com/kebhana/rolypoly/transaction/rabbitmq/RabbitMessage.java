package com.kebhana.rolypoly.transaction.rabbitmq;

import lombok.Data;

@Data
public class RabbitMessage<T> {
	private T t;
}
